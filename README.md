# ** Site da aplicação ** #
  
* https://flaskcrudestoque.herokuapp.com/  
  
# ** Ferramentas utilizadas ** #
* Python 3.8.5
* Jinja
* Flask
* Linux Mint Cinnamon    
  .. Para publicar o aplicativo no Heroku:    
* Gunicorn   

# ** Como executar a aplicação (Linux) ? ** #

* Criando e executando o ambiente virtual:  

1. com o terminal aberto, entre na pasta do projeto
2. $ python3 -m venv env  
3. $ source env/bin/activate  (para ativar o ambiente virtual)  

* Instalando o Flask:  

1. com o ambiente virtual ativado, execute:  
2. $ pip install flask    
  
* Instalando o SQLAlchemy: 
 
1. com o ambiente virtual ativado, execute:  
2. $ pip install flask_sqlalchemy  

* Criando o database SQLAlchemy:  

1. entre na pasta do projeto com o virtual env criado no passo anterior ativado.  
2. $ python3      
3. $ from app import db     
4. $ db.create_all()    
5. $ quit (para sair do Python)  

* Executando a aplicação:
    
1. $ python3 app.py  
2. abra no navegador o endereço: http://127.0.0.1:5000/  
3. fim    

# ** Porque trabalhar na WPensar? ** #

* Porque quero aprender coisas novas a cada minuto.

* Porque quero ajudar a construir a maior empresa de tecnologia para educação do Brasil.

* Porque tenho sede por novos desafios.

* Porque quero fazer diferença!

# ** Como faço para me candidatar? ** #

1. Faça um fork do repositório
2. Desenvolva o desafio de acordo com o proposto abaixo
3. Mande um pull request com o curriculo e a resposta do desafio

## ** Caso você não deseje que o envio seja público ** ##

1. Faça um clone do repositório
2. Desenvolva o desafio de acordo com o proposto abaixo
3. Envie um email com o curriculo e um arquivo patch para rh@wpensar.com.br

# **Desafio:** #

O conceito desse desafio é nos ajudar a avaliar as habilidades dos candidatos às vagas de backend.

Você tem que desenvolver um sistema de estoque para um supermercado.

Esse supermercado assume que sempre que ele compra uma nova leva de produtos, ele tem que calcular o preço médio de compra de cada produto para estipular um preço de venda.
Para fins de simplificação assuma que produtos que tenham nomes iguais, são o mesmo produto e que não existe nem retirada e nem venda de produtos no sistema.

O valor calculado de preço médio deve ser armazenado.

Seu sistema deve:

1. Cadastro de produtos (Nome)
2. Compra de produtos (Produto, quantidade e preço de compra)
3. Listagem dos produtos comprados separados por compra (Nome, quantidade, preço de compra, preço médio)
4. Ser fácil de configurar e rodar em ambiente Unix (Linux ou Mac OS X)
5. Ser WEB
6. Ser escrita em Python 3.7+
7. Só deve utilizar biliotecas livres e gratuitas

Esse sistema não precisa ter, mas será um plus:

1. Autenticação e autorização (se for com OAuth, melhor ainda)
2. Ter um design bonito
3. Testes automatizados
4. Usar Jinja Template Engine para fazer o Front End.
5. Disponibilizar o Projeto na Nuvem(Heroku, Aws, ou similar)

# **Avaliação:** #

Vamos avaliar seguindo os seguintes critérios:

1. Você conseguiu concluir os requisitos?
2. Você documentou a maneira de configurar o ambiente e rodar sua aplicação?
