from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)

# Dizendo ao aplicativo onde o database está localizado | Inicializando o database.
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)

class Produto(db.Model):
    # criando as colunas do database.
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    completed = db.Column(db.Integer, default=0)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)
    med_compra = db.Column(db.Numeric(precision=2), default=0) 
    med_venda = db.Column(db.Numeric(precision=2), default=0) 

    def __repr__(self):
        return '<prod %r>' % self.id

    def calc_med_comp(self):
        remessas = Remessa.query.filter_by(content = self.content).all()
        som_val = 0.0
        cnt_rem = 0
        for remessa in remessas:
            som_val += remessa.price
            cnt_rem += 1
        if cnt_rem != 0:
            self.med_compra = som_val / cnt_rem
        else:
            self.med_compra = 0
    
    def calc_med_venda(self):
        self.med_venda = self.med_compra * (130/100)  # 30% de lucro :)


class Remessa(db.Model):
    # criando as colunas do database.
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(200), nullable=False)
    completed = db.Column(db.Integer, default=0)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    quantity = db.Column(db.Integer)
    price = db.Column(db.Integer) # preço unitário do produto.
    tot_price = db.Column(db.Integer)

    # retorna string cada vez que criarmos um elemento.
    def __repr__(self):
        return '<Remessa %r>' % self.id


@app.route('/', methods=['POST', 'GET'])
def index():
    if request.method == 'POST':
        # cadastrando produto.
        prod_content = request.form['content']
        if prod_content == '':
            return 'Entrada em branco. Tente novamente'

        new_product = Produto(content=prod_content)
        
        # exportando os dados para o database.
        try:
            db.session.add(new_product)
            db.session.commit()
            return redirect('/')
        except:
            return 'Erro ao salvar a tarefa.'
    else:
        # mostrando os elementos por ordem de criação.
        products = Produto.query.order_by(Produto.date_created).all()
        remessas = Remessa.query.order_by(Remessa.date_created).all()
        return render_template('index.html', products=products, remessas=remessas)


# deletando produto.
@app.route('/delete/<int:id>')
def delete(id):
    prods_to_delete = Produto.query.get_or_404(id)
    
    try:
        db.session.delete(prods_to_delete)
        db.session.commit()
        return redirect('/')
    except:
        return 'Erro ao deletar a tarefa'

# editando produto.
@app.route('/update/<int:id>', methods=['GET', 'POST'])
def update(id):
    prod = Produto.query.get_or_404(id)
    if request.method == 'POST':
        # tentando.
        prod.content = request.form['content']
        try:
            db.session.commit()
            return redirect('/')
        except:
            return 'Erro ao editar tarefa.'        
    else:
        return render_template('update.html', task=prod)

# comprando remessa de um produto pelo seu id.
@app.route('/buy/<int:id>', methods=['GET', 'POST'])
def buy(id):
    prod = Produto.query.get_or_404(id)
    if request.method == 'POST':
        remessa = Remessa(content=prod.content)
        
        remessa.quantity = request.form['quantity']
        remessa.price = request.form['price']

        if remessa.quantity == '' or remessa.price == '':
            return 'Entrada em branco. Tente novamente'
        
        remessa.tot_price = int(remessa.quantity) * int(remessa.price)
        
        try:
            db.session.add(remessa)
            db.session.commit()
            prod.calc_med_comp()
            prod.calc_med_venda()
            db.session.commit()

            return redirect('/')
        except:
            return 'Erro ao comprar remessa.'
    else:
        return render_template('buy.html', task=prod)
         

# listando remessa selecionada pelo id do produto.
@app.route('/listar-remessas/<int:id>')
def listar_remessas(id):    
    product = Produto.query.get_or_404(id)
    remessas = Remessa.query.filter_by(content = product.content).all()
    return render_template('remessas.html', remessas=remessas)


if __name__ == "__main__":
    app.run(debug=True)